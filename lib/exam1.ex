defmodule Exam1 do

  def mean(list) do
    Enum.sum(list) / length(list)
  end

  def median([]), do: nil
  def median(list) do
    midpoint = (length(list) / 2)
    |> Float.floor()
    |> round

    {l1, l2} = Enum.sort(list)
    |> Enum.split(midpoint)

    case length(l2) > length(l1) do
      true -> [med | _] = l2
      med

      false -> [m1 | _] = l2
      [m2 | _] = Enum.reverse(l1)
      mean([m1, m2])
    end
  end

  def quartile1([]), do: nil
  def quartile1(list) do
    q1 = (length(list) + 1) / 4

    if is_float(q1) do
      split = floor(q1)
      {l1, l2} = Enum.sort(list)
      |> Enum.split(split)
      first = Enum.at(l1, split - 1)
      second = hd(l2)
      quartile = (first + second) / 2
      quartile

      else
      {l1, l2} = Enum.sort(list)
      |> Enum.split(q1)
      hd(l2)
    end
  end


  def quartile3([]), do: nil
  def quartile3(list) do
    q3 = 3 * (length(list)) / 4

    if is_float(q3) do
      split = floor(q3)
      {l1, l2} = Enum.sort(list)
      |> Enum.split(split)
      first = Enum.at(l1, split - 1)
      second = hd(l2)
      quartile = (first + second) / 2
      quartile

      else
      {l1, l2} = Enum.sort(list)
      |> Enum.split(q3)
      hd(l2)
    end
  end


  def iqr(list) do
    quartile3(list) - quartile1(list)
  end
end
