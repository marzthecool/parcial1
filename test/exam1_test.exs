defmodule Exam1Test do
  use ExUnit.Case
  doctest Exam1

  # @tag :skip
  test "Median" do
    l1 = [30, 20, 14, 4, 1, 16, 20, 3, 10, 23, 36]
    l2 = [2, 44, 28, 46, 25, 24, 20, 23, 12, 11, 43, 16, 45, 11, 29, 26]
    assert Exam1.median(l1) == 16
    assert Exam1.median(l2) == 24.5
  end

  # @tag :skip
  test "Q1" do
    l1 = [30, 20, 14, 4, 1, 16, 20, 3, 10, 23, 36]
    l2 = [2, 44, 28, 46, 25, 24, 20, 23, 12, 11, 43, 16, 45, 11, 29, 26]
    assert Exam1.quartile1(l1) == 7.0
    assert Exam1.quartile1(l2) == 14.0
  end

  # @tag :skip
  test "Q3" do
    l1 = [30, 20, 14, 4, 1, 16, 20, 3, 10, 23, 36]
    l2 = [2, 44, 28, 46, 25, 24, 20, 23, 12, 11, 43, 16, 45, 11, 29, 26]
    assert Exam1.quartile3(l1) == 21.5
    assert Exam1.quartile3(l2) == 36
  end

  # @tag :skip
  test "IQR" do
    l1 = [30, 20, 14, 4, 1, 16, 20, 3, 10, 23, 36]
    l2 = [2, 44, 28, 46, 25, 24, 20, 23, 12, 11, 43, 16, 45, 11, 29, 26]
    assert Exam1.iqr(l1) == 14.5
    assert Exam1.iqr(l2) == 22
  end
end
